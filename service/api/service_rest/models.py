from django.db import models
from django.urls import reverse


class Technician(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    employee_id = models.PositiveIntegerField(unique=True)

    def __str__(self):
        return self.first_name

    def get_api_url(self):
        return reverse("api_show_technician", kwargs={"pk": self.id})


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=100)
    sold = models.BooleanField(default=False)
    import_href = models.URLField(max_length=200, unique=True)

    def __str__(self):
        return self.vin


class Appointment(models.Model):
    date_time = models.DateTimeField()
    reason = models.CharField(max_length=100 )
    status = models.CharField(max_length=100)
    vin = models.CharField(max_length=100)
    customer = models.CharField(max_length=100)
    vip = models.BooleanField(default=False, null=True)

    technician = models.ForeignKey(
        Technician,
        related_name="appointments",
        on_delete=models.CASCADE
    )

    def __str__(self):
        return self.customer

    def get_api_url(self):
        return reverse("api_appointment", kwargs={"pk": self.pk})

    def cancel(self):
        status = "canceled"
        self.status = status
        self.save()

    def finished(self):
        status = "finished"
        self.status = status
        self.save()
