# Generated by Django 4.0.3 on 2023-07-28 19:25

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('service_rest', '0002_alter_appointment_customer_alter_appointment_reason_and_more'),
    ]

    operations = [
        migrations.AddField(
            model_name='appointment',
            name='vip',
            field=models.BooleanField(default=False, null=True),
        ),
    ]
