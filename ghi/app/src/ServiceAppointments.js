import React, { useState, useEffect } from "react";


function ServiceList() {
  const [appointments, setAppointments] = useState([]);

  const fetchData = async () => {
    const url = `http://localhost:8080/api/appointments/`;
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setAppointments(data.appointments);
    }
  };
  useEffect(() => {
    fetchData();
  }, []);

  function handleCancel(id) {
    let url = `http://localhost:8080/api/appointments/${id}/cancel/`;
    window.location.reload();
    fetch(url, {
      method: "PUT",
    });
  }

  function handleDelete(id) {
    let url = `http://localhost:8080/api/appointments/${id}/finish/`;
    window.location.reload();
    fetch(url, {
      method: "PUT",
    });
  }

  return (
    <div className="shadow p-4 mt-4">
      <h1>Service Appointments </h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Vin</th>
            <th>Vip</th>
            <th>Customer</th>
            <th>Date</th>
            <th>Time</th>
            <th>Technician</th>
            <th>Reason</th>
            <th>Cancel/Finnish</th>
          </tr>
        </thead>
        <tbody>
          {appointments.map((appointment) => {
            if (!appointment.status)
              return (
                <tr key={appointment.id}>
                  <td>{appointment.vin}</td>
                  <td>{appointment.vip.toString()}</td>
                  <td>{appointment.customer}</td>
                  <td>
                    {new Date(appointment.date_time).toLocaleDateString()}
                  </td>
                  <td>
                    {new Date(appointment.date_time).toLocaleTimeString()}
                  </td>
                  <td>
                    {appointment.technician.first_name}{" "}
                    {appointment.technician.last_name}
                  </td>
                  <td>{appointment.reason}</td>
                  <td>
                    <button
                      onClick={() => handleCancel(appointment.id)}
                      className="btn btn-primary"
                      style={{
                        color: "#FFFFFF",
                        backgroundColor: "#E05875",
                        border: "1px solid #E05875",
                      }}>
                      Cancel
                    </button>
                    <button
                      onClick={() => handleDelete(appointment.id)}
                      className="btn btn-primary"
                      style={{
                        color: "#FFFFFF",
                        backgroundColor: "#9ACD32",
                        border: "1px solid #9ACD32",
                      }}>
                      Finnish
                    </button>
                  </td>
                </tr>
              );
          })}
        </tbody>
      </table>
    </div>
  );
}

export default ServiceList;
