function SalesList(props) {
  const { sales } = props;
  return (
    <div className="shadow p-4 mt-4">
      <h1>Sales</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Salesperson Employee Id</th>
            <th>Salesperson Name</th>
            <th>Customer</th>
            <th>VIN</th>
            <th>Price</th>
          </tr>
        </thead>
        <tbody>
          {sales.map((sale) => {
            return (
              <tr key={sale.href}>
                <td>{sale.salesperson.employee_id}</td>
                <td>{sale.salesperson.first_name}</td>
                <td>{sale.customer.first_name}</td>
                <td>{sale.automobile.vin}</td>
                <td>${sale.price}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}

export default SalesList;
