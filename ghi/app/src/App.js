import { BrowserRouter, Routes, Route } from "react-router-dom";
import MainPage from "./MainPage";
import Nav from "./Nav";
import TechnicianForm from "./TechnicianForm";
import TechnicianList from "./TechnicianList";
import ServiceForm from "./ServiceForm";
import SalespeopleList from "./SalespeopleList";
import SalespersonForm from "./SalespersonForm";
import CustomersList from "./CustomersList";
import CustomerForm from "./CustomerForm";
import SalesList from "./SalesList";
import SalespersonHistoryList from "./SalespersonHistoryList";
import ServiceAppointments from "./ServiceAppointments";
import SaleForm from "./SaleForm";
import ServiceHistory from "./ServiceHistory";
import ManufacturerList from "./ManufacturerList";
import ModelsList from "./ModelsList";
import ManufacturerForm from "./ManufacturerForm";
import CreateVehicleForm from "./CreateVehicleForm";
import AutomobileForm from "./AutomobileForm";
import AutoList from "./AutoList";


function App(props) {
  if (props.data === undefined) {
    return null;
  }

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/vehicle/new" element={<CreateVehicleForm />} />
          <Route path="/autos" element={<AutoList />} />
          <Route path="/technician/new" element={<TechnicianForm />} />
          <Route path="/technicians" element={<TechnicianList />} />
          <Route path="/service/new" element={<ServiceForm />} />
          <Route path="/services" element={<ServiceAppointments />} />
          <Route path="/services" element={<ServiceAppointments />} />
          <Route path="/service/history" element={<ServiceHistory />} />
          <Route
            path="/salespeople"
            element={<SalespeopleList salespeople={props.data.salespeople} />}
          />
          <Route path="/salespeople/create" element={<SalespersonForm />} />
          <Route
            path="/customers"
            element={<CustomersList customers={props.data.customers} />}
          />
          <Route path="/customers/create" element={<CustomerForm />} />
          <Route
            path="/sales"
            element={<SalesList sales={props.data.sales} />}
          />
          <Route
            path="/sales/create"
            element={<SaleForm data={props.data} />}
          />
          <Route
            path="/sales/history"
            element={<SalespersonHistoryList sales={props.data.sales} />}
          />
          <Route
            path="/manufacturers"
            element={
              <ManufacturerList manufacturers={props.data.manufacturers} />
            }
          />
          <Route path="/manufacturers/create" element={<ManufacturerForm />} />
          <Route
            path="/models"
            element={<ModelsList models={props.data.models} />}
          />
          <Route
            path="/automobiles/create"
            element={<AutomobileForm data={props.data.models} />}
          />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
