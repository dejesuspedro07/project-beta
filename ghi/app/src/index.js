import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';


const root = ReactDOM.createRoot(document.getElementById('root'));

async function loadData() {
  const salespeopleResponse = await fetch('http://localhost:8090/api/salespeople/');
  let salespeopleData = null;
  if (salespeopleResponse.ok) {
    salespeopleData = await salespeopleResponse.json();
  }

  const customersResponse = await fetch('http://localhost:8090/api/customers/')
  let customersData = null;
  if (customersResponse.ok) {
    customersData = await customersResponse.json();
  }

  const salesResponse = await fetch('http://localhost:8090/api/sales/')
  let salesData = null;
  if (salesResponse.ok) {
    salesData = await salesResponse.json();
  }

  const manufacturerResponse = await fetch('http://localhost:8100/api/manufacturers/')
  let manufacturersData = null;
  if (manufacturerResponse.ok) {
    manufacturersData = await manufacturerResponse.json()
  }

  const modelsResponse = await fetch('http://localhost:8100/api/models/')
  let modelsData = null;
  if (modelsResponse.ok) {
    modelsData = await modelsResponse.json()
  }

  root.render (
    <React.StrictMode>
      <App data = { {'salespeople': salespeopleData, 'customers': customersData, 'sales': salesData, 'manufacturers': manufacturersData, 'models': modelsData} } />
    </React.StrictMode>
  )
}

loadData();
