import React, { useState } from "react";


function ManufacturerForm() {
  const [manufacturer, setManufacturer] = useState("");

  const handleSubmit = async () => {
    const data = {};
    data.name = manufacturer;

    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    
    const response = await fetch(
      "http://localhost:8100/api/manufacturers/",
      fetchConfig
    );

    if (response.ok) {
      setManufacturer("");
    }
  };

  const handleManufacturerChange = async (event) => {
    const value = event.target.value;
    setManufacturer(value);
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a Manufacturer</h1>
          <form onSubmit={handleSubmit} id="add-salesperson-form">
            <div className="form-floating mb-3">
              <input
                value={manufacturer}
                onChange={handleManufacturerChange}
                placeholder="Manufacturer name"
                required
                type="text"
                name="manufacturer"
                id="manufacturer"
                className="form-control"
              />
              <label htmlFor="manufacturer">Manufacturer name...</label>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default ManufacturerForm;
