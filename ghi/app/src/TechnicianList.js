import React, { useState, useEffect } from "react";


function TechnicianList() {
  const [technicians, setTechnicians] = useState([]);

  const fetchData = async () => {
    const url = `http://localhost:8080/api/technicians/`;
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setTechnicians(data);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);
  return (
    <div className="shadow p-4 mt-4">
      <h1>Technicians </h1>

      <table className="table table-striped">
        <thead>
          <tr>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Employee ID</th>
          </tr>
        </thead>
        <tbody>
          {technicians.map((technician) => {
            return (
              <tr key={technician.id}>
                <td>{technician.first_name}</td>
                <td>{technician.last_name}</td>
                <td>{technician.employee_id}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}

export default TechnicianList;
