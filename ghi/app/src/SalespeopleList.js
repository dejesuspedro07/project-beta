function SalespeopleList(props) {
  const { salespeople } = props;
  return (
    <div className="shadow p-4 mt-4">
      <h1>Salespeople</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Employee Id</th>
          </tr>
        </thead>
        <tbody>
          {salespeople.map((salesperson) => {
            return (
              <tr key={salesperson.id}>
                <td>{salesperson.first_name}</td>
                <td>{salesperson.last_name}</td>
                <td>{salesperson.employee_id}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}

export default SalespeopleList;
