function ModelsList(props) {
  const { models } = props;
  return (
    <div className="shadow p-4 mt-4">
      <h1>Models</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Name</th>
            <th>Manufacturer</th>
            <th>Picture</th>
          </tr>
        </thead>
        <tbody>
          {models.models.map(model => {
            return (
              <tr key={model.href}>
                <td>{ model.name }</td>
                <td>{ model.manufacturer.name }</td>
                <img style={{ width: '100%' }} src={ model.picture_url } alt='image of a vehicle'/>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  )
}

export default ModelsList;
