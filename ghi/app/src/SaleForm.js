import React, { useEffect, useState } from "react";


function SaleForm(props) {
  const [price, setPrice] = useState("");
  const [automobiles, setAutomobiles] = useState([]);
  const [automobile, setAutomobile] = useState("");
  const [salesperson, setSalesperson] = useState("");
  const [customer, setCustomer] = useState("");
  const { customers, salespeople, sales } = props.data;

  const fetchData = async () => {
    const url = "http://localhost:8100/api/automobiles/";
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setAutomobiles(data.autos);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  const handleAutomobileChange = async (event) => {
    const value = event.target.value;
    setAutomobile(value);
  };

  const handleSalespersonChange = async (event) => {
    const value = event.target.value;
    setSalesperson(value);
  };

  const handleCustomerChange = async (event) => {
    const value = event.target.value;
    setCustomer(value);
  };

  const handlePriceChange = async (event) => {
    const value = event.target.value;
    setPrice(value);
  };

  const getCustomer = (customers) => {
    return customers.find((user) => user.id == customer);
  };

  const getSalesperson = (people) => {
    return people.find((someone) => someone.employee_id == salesperson);
  };

  const isAutoSold = (autos) => {
    return autos.find((car) => {
      return car.automobile.vin == automobile;
    });
  };

  const handleSubmit = async (event) => {
    const data = {};
    data.price = price;

    const cust = getCustomer(customers);
    data.customer = {
      first_name: cust.first_name,
      last_name: cust.last_name,
      address: cust.address,
      phone_number: cust.phone_number,
    };

    const worker = getSalesperson(salespeople);
    data.salesperson = {
      first_name: worker.first_name,
      last_name: worker.last_name,
      employee_id: worker.employee_id,
    };

    const isSold = isAutoSold(sales);
    data.automobile = {
      vin: automobile,
      sold: isSold?.automobile?.sold,
    };

    const url = "http://localhost:8090/api/sales/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      setAutomobile([]);
      setSalesperson([]);
      setCustomer([]);
      setPrice("");
    }
  };
  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Record a new sale</h1>
          <form onSubmit={handleSubmit} id="create-hat-form">
            <div className="mb-3">
              <select
                value={automobile}
                onChange={handleAutomobileChange}
                required
                name="vin"
                id="vin"
                className="form-select"
              >
                <option value="">Choose an automobile VIN...</option>
                {automobiles.map((automobile) => {
                  return (
                    <option key={automobile.vin} value={automobile.vin}>
                      {automobile.vin}
                    </option>
                  );
                })}
              </select>
            </div>

            <div className="mb-3">
              <select
                value={salesperson}
                onChange={handleSalespersonChange}
                required
                name="salesperson"
                id="salesperson"
                className="form-select"
              >
                <option value="">Choose a salesperson...</option>
                {salespeople.map((salesperson) => {
                  return (
                    <option
                      key={salesperson.employee_id}
                      value={salesperson.employee_id}
                    >
                      {salesperson.first_name} {salesperson.last_name}, Id{" "}
                      {salesperson.employee_id}
                    </option>
                  );
                })}
              </select>
            </div>

            <div className="mb-3">
              <select
                value={customer}
                onChange={handleCustomerChange}
                required
                name="customer"
                id="customer"
                className="form-select"
              >
                <option value="">Choose a customer...</option>
                {customers.map((customer) => {
                  return (
                    <option key={customer.id} value={customer.id}>
                      {customer.first_name}
                    </option>
                  );
                })}
              </select>
            </div>

            <div className="form-floating mb-3">
              <input
                value={price}
                onChange={handlePriceChange}
                placeholder="price"
                required
                type="number"
                name="price"
                id="picture"
                className="form-control"
              />
              <label htmlFor="price">Price</label>
            </div>

            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default SaleForm;
