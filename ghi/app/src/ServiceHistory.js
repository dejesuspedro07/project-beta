import React, { useState, useEffect } from "react";


function ServiceHistory() {
  const [Vin, setVin] = useState("");
  const [search, setSearch] = useState("");
  const [appointments, setAppointments] = useState([]);

  const fetchData = async () => {
    const url = `http://localhost:8080/api/appointments/`;
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setAppointments(data.appointments);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  function handleSearch(event) {
    const value = event.target.value;
    setVin(value);
  }

  function handleSearchSubmit(event) {
    event.preventDefault();
    setSearch(Vin);
    setVin("");
  }

  function filter() {
    if (search.length > 0) {
      return appointments.filter((appointment) => appointment.vin === search);
    } else {
      return appointments;
    }
  }

  return (
    <div>
      <h1>Service History</h1>
      <form onSubmit={handleSearchSubmit}>
        <div className="input-group mb-3">
          <input
            type="text"
            value={Vin}
            onChange={handleSearch}
            className="form-control"
          />
          <button className="btn btn-primary">Search</button>
        </div>
      </form>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Vin</th>
            <th>Customer</th>
            <th>Date</th>
            <th>Time</th>
            <th>Technician</th>
            <th>Reason</th>
            <th>Status</th>
          </tr>
        </thead>
        <tbody>
          {filter().map((appointment) => {
            return (
              <tr key={appointment.id}>
                <td>{appointment.vin}</td>
                <td>{appointment.customer}</td>
                <td>{new Date(appointment.date_time).toLocaleDateString()}</td>
                <td>{new Date(appointment.date_time).toLocaleTimeString()}</td>
                <td>
                  {appointment.technician.first_name}{" "}
                  {appointment.technician.last_name}
                </td>
                <td>{appointment.reason}</td>
                <td>{appointment.status}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}

export default ServiceHistory;
