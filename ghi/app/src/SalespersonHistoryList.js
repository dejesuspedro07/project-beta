import React, { useEffect, useState } from "react";


function SalespersonHistoryList({ sales }) {
  const [salesperson, setSalesperson] = useState("");
  const [salesTeam, setSalesTeam] = useState([]);

  const handleSalespersonChange = async (event) => {
    const value = event.target.value;
    setSalesperson(value);
  };

  const fetchData = async (event) => {
    const url = "http://localhost:8090/api/salespeople/";
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setSalesTeam(data);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className="shadow p-4 mt-4">
      <h1>Sales</h1>
      <div className="mb-3">
        <select
          onChange={handleSalespersonChange}
          required
          name="salesperson"
          id="salesperson"
          className="form-select">
          <option value="">Choose a salesperson</option>
          {salesTeam.map((employee) => {
            return (
              <option key={employee.employee_id} value={employee.employee_id}>
                {employee.first_name}
              </option>
            );
          })}
        </select>
      </div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Salesperson Name</th>
            <th>Customer</th>
            <th>VIN</th>
            <th>Price</th>
          </tr>
        </thead>
        <tbody>
          {sales.filter((sale) =>
              salesperson
                ? sale.salesperson.employee_id.toString() === salesperson
                : sale).map((sale) => {
              return (
                <tr key={sale.href}>
                  <td>{sale.salesperson.first_name}</td>
                  <td>{sale.customer.first_name}</td>
                  <td>{sale.automobile.vin}</td>
                  <td>${sale.price}</td>
                </tr>
              );
            })}
        </tbody>
      </table>
    </div>
  );
}

export default SalespersonHistoryList;
