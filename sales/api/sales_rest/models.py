from django.db import models
from django.urls import reverse


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False)
    import_href = models.URLField(max_length=100, unique=True)


class Salesperson(models.Model):
  first_name = models.CharField(max_length=100)
  last_name = models.CharField(max_length=100)
  employee_id = models.PositiveSmallIntegerField()

  def __str__(self):
    return f'{self.first_name} {self.employee_id}'


class Customer(models.Model):
  first_name = models.CharField(max_length=50)
  last_name = models.CharField(max_length=50)
  address = models.CharField(max_length=100)
  phone_number = models.CharField(max_length=20)

  def __str__(self):
    return f'{self.first_name} {self.last_name}'


class Sale(models.Model):
  automobile = models.ForeignKey(AutomobileVO, related_name="sales", on_delete=models.CASCADE)
  salesperson = models.ForeignKey(Salesperson, related_name="sales", on_delete=models.CASCADE)
  customer = models.ForeignKey(Customer, related_name="sales", on_delete=models.CASCADE)
  price = models.DecimalField(max_digits=10, decimal_places=2)

  def sell(self):
    if not self.automobile.sold:
      self.automobile.sold = True
      self.automobile.save()
      self.save()

  def get_api_url(self):
    return reverse('api_show_sale', kwargs={'pk': self.pk})

  def __str__(self):
    return f'{self.salesperson} sold {self.automobile} to {self.customer} for ${self.price}.'
