from decimal import Decimal
from json import JSONEncoder
from common.json import ModelEncoder
from .models import Sale, AutomobileVO, Salesperson, Customer


class DecimalEncoder(JSONEncoder):
  def default(self, o):
    if isinstance(o, Decimal):
      return int(o)
    else:
      return int(o)


class CustomerDetailEncoder(ModelEncoder):
  model = Customer
  properties = [
    'first_name',
    'last_name',
    'address',
    'phone_number',
    'id'
  ]


class SalespersonDetailEncoder(ModelEncoder):
  model = Salesperson
  properties = [
    'first_name',
    'last_name',
    'employee_id'
  ]


class AutomobileVODetailEncoder(ModelEncoder):
  model = AutomobileVO
  properties = [
    'vin',
    'sold'
  ]


class SaleDetailEncoder(ModelEncoder):
  model = Sale
  properties = [
    'price',
    'automobile',
    'salesperson',
    'customer'
  ]
  encoders = {
    'automobile': AutomobileVODetailEncoder(),
    'salesperson': SalespersonDetailEncoder(),
    'customer': CustomerDetailEncoder(),
    'price': DecimalEncoder()
  }
